var app = angular.module("desafioApp", ['ui.bootstrap']);
app.controller("desafioController", ['$scope', '$modal', '$http',

    function ($scope, $modal, $http) {

        $scope.showForm = function (id) {
            var modalInstance = $modal.open({
                templateUrl: 'modal-form.html',
                controller: desafioModalController,
                scope: $scope,
                resolve: {
                    pessoaEdit: function () {
                        return id;
                    }
                }
            });
        };

        var urlBase = "http://localhost:8080/api/v1/pessoa/"
        $scope.pessoas = [];
        $scope.pessoa = {};
        var carregarPessoas = function () {
            $http.get(urlBase + "listar").
                then(function (response) {
                    $scope.pessoas = response.data;
                }).catch(function (response) {
                    alert('Ocorreu algum erro no servidor');
                    console.log(response.status, response.data);
                });
        };

        $scope.adicionarPessoa = function (pessoa) {
            $http.post(urlBase + "salvar", pessoa).
                then(function (response) {
                    delete $scope.pessoa;
                    carregarPessoas();
                }).catch(function (response) {
                    alert('Ocorreu algum erro no servidor');
                    console.log(response.status, response.data);
                });
        }

        $scope.apagarPessoas = function (pessoas) {
            var resposta = confirm("Deseja realmente apagar as pessoas selecionadas?");
            if (resposta) {
                var pessoasSelecionados = pessoas.filter(function (pessoa) {
                    if (pessoa.selecionado) {
                        return pessoa;
                    }
                }).map(function (p) {
                    return p.id;
                });
                $http({
                    method: 'DELETE',
                    url: urlBase + "remover",
                    hasBody: true,
                    data: pessoasSelecionados,
                    headers: { 'Content-Type': 'application/json;charset=utf-8' }
                }).then(function (response) {
                    carregarPessoas();
                }).catch(function (response) {
                    alert('Ocorreu algum erro no servidor');
                    console.log(response.status, response.data);
                });
            }
        }

        carregarPessoas();


        $scope.isPessoaSelecionada = function (pessoas) {
            return pessoas.some(function (pessoa) {
                return pessoa.selecionado;
            });
        }

        $scope.isUmaPessoaSelecionada = function (pessoas) {
            var pessoasSelecionados = pessoas.filter(function (pessoa) {
                if (pessoa.selecionado) {
                    return pessoa;
                }
            });
            return pessoasSelecionados.length == 1;
        }

        $scope.pessoaSelecionada = function (pessoas) {
            var pessoasSelecionados = pessoas.filter(function (pessoa) {
                if (pessoa.selecionado) {
                    return pessoa;
                }
            });
            return pessoasSelecionados[0];

        }




    }]);

var desafioModalController = function ($scope, $modalInstance, pessoaEdit) {

    $scope.pessoa = {}
    $scope.titulo = "";
    if (pessoaEdit === null || pessoaEdit === undefined) {
        $scope.titulo = "Cadastrar Pessoa";
    } else {
        $scope.pessoa.nome = pessoaEdit.nome;
        $scope.pessoa.idade = pessoaEdit.idade;
        if(pessoaEdit.sexo === "MASCULINO"){
            $scope.pessoa.sexo = '0';
        }else{
            $scope.pessoa.sexo = '1';
        }
        $scope.pessoa.id = pessoaEdit.id;
        $scope.titulo = "Alterar Pessoa";
    }
    $scope.form = {}
    $scope.submitForm = function () {
        $scope.adicionarPessoa($scope.pessoa);
        $modalInstance.close('closed');
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};