package br.com.cronapp.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.cronapp.model.Pessoa;
import br.com.cronapp.service.PessoaService;

@RestController
@ComponentScan
@RequestMapping("/api/v1/pessoa")
public class PessoaController {

	@Autowired
	private PessoaService pessoaService;

	@RequestMapping(path = "/salvar", method = RequestMethod.POST)
	ResponseEntity<Object> salvar(@RequestBody Pessoa pessoa) {
		try {
			pessoaService.salvar(pessoa);
			return new ResponseEntity<Object>(pessoa, HttpStatus.OK);
		} catch (Exception e) {			
			return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(path = "/listar", method = RequestMethod.GET)
	ResponseEntity<Object> listar() {
		try {
			Iterable<Pessoa> pessoas = pessoaService.listar();
			return new ResponseEntity<Object>(pessoas, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(path = "/remover", method = RequestMethod.DELETE)
	ResponseEntity<Object> remover(@RequestBody Integer... ids) {
		try {
			Iterable<Pessoa> pessoas = pessoaService.buscarPorIds(Arrays.asList(ids));
			pessoaService.remover(pessoas);
			return new ResponseEntity<Object>(pessoas, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
