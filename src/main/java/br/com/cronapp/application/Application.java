package br.com.cronapp.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan(basePackages = {"br.com.cronapp"})
@EntityScan("br.com.cronapp.model")
@EnableJpaRepositories("br.com.cronapp.repository")
public class Application  {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
}