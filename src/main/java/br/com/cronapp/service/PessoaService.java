package br.com.cronapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cronapp.model.Pessoa;
import br.com.cronapp.repository.PessoaRepository;

@Service
public class PessoaService {

	@Autowired
	private PessoaRepository pessoaRepository;

	public void salvar(Pessoa pessoa) {
		pessoaRepository.save(pessoa);
	}

	public void remover(Iterable<Pessoa> pessoa) {
		pessoaRepository.delete(pessoa);
	}

	public Iterable<Pessoa> listar() {
		return pessoaRepository.findAll();
	}
	
	public Iterable<Pessoa> buscarPorIds(Iterable<Integer> ids) {
		return pessoaRepository.findAll(ids);
	}
}
