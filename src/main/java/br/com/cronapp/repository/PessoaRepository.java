package br.com.cronapp.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.cronapp.model.Pessoa;

public interface PessoaRepository extends CrudRepository<Pessoa, Integer> {
	
}
